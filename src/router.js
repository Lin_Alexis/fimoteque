import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import ListeFiltre from './views/ListeFiltre.vue'
import VosFilms from './views/VosFilms.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/ListeFiltre',
            name: 'ListeFiltre',
            component: ListeFiltre,
        },
        {
            path: '/VosFilms',
            name: 'VosFilms',
            component: VosFilms,
        },

    ]
})
