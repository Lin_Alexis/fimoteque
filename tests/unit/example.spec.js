import {shallowMount} from '@vue/test-utils'
import Search from '@/components/Search.vue'

describe('Search.vue', () => {
    it('renders props.msg when passed', () => {
        const searchValue = 'warcraft';
        const wrapper = shallowMount(Search, {
            propsData: {
                searchValue,
                results: [],
            }
        })
        wrapper.search();
         expect(wrapper.results.length).toBe(3)
    })
})
